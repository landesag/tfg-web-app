var ExtractTextPlugin = require('extract-text-webpack-plugin');
var WebpackCleanupPlugin = require('webpack-cleanup-plugin');

var isProductionBuild = process.env.NODE_ENV === 'production';

var loaders = [
    {
        exclude: /node_modules/,
        test: /\.js/,
        loader: 'babel',
        query: {
            cacheDirectory: true,
            presets: ['react', 'es2015', 'stage-0'],
            plugins: ['transform-decorators-legacy']
        }
    },{
        test: /\.html$/,
        loader: 'file',
        query:{
            name: '[name].[ext]'
        }
    },{
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
            'style-loader',
            'css-loader!sass-loader')
    },
    {
        test: /\.(png|jpg|svg)$/,
        loader: 'url-loader?limit=8192'
    }
];

var plugins = [
    new ExtractTextPlugin('styles.css', {
        allChunks: true
    })
];

if (!isProductionBuild){
    //En desarrollo usamos la recarga rapida de componentes de react
    loaders.unshift({
        exclude: /node_modules/,
        test: /\.js/,
        loader: 'react-hot',
    })
} else {
    //Antes del empaquetado para produccion eliminamos el contenido de la carpeta dist
    plugins.unshift(new WebpackCleanupPlugin({}))
}


module.exports = {
    cache: true,
    entry:{
        javascript: './index.js',
        html: './index.html',
        css: './styles/styles.scss'
    },
    output: {
        filename: 'app.js',
        path: '/dist',
    },
    devtool: isProductionBuild ? false : 'source-map',
    module: {
        loaders: loaders
    },
    plugins : plugins
};
