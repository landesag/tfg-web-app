import React, { Component, PropTypes } from 'react'
import { render } from 'react-dom'
import Dimensions from 'react-dimensions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import autobind from 'autobind-decorator'
import Avatar from 'material-ui/Avatar';
import FileFolder from 'material-ui/svg-icons/file/folder';
import styles from 'material-ui/styles';
import FontIcon from 'material-ui/FontIcon';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';

import * as colors from 'material-ui/styles/colors';


import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';
import CardText from 'material-ui/Card/CardText';
import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router'

import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';
import * as TaskActions from '../store/actions/TaskActions'


@Dimensions()
class Home extends Component {

  static contextTypes = { router: React.PropTypes.object.isRequired };



  startTask = (task, executionId, caseId) => {
    localStorage.setItem('task', JSON.stringify(task))
    localStorage.setItem('executionId', executionId)
    localStorage.setItem('caseId', caseId)
    this.context.router.push('/task')
  };

  componentDidMount() {

  }


  render() {

    const { tasks, isLookingForTasks } = this.props;

    let iniciarTest = this.iniciarTest
    console.log(tasks)
    if (isLookingForTasks) {
      // Note that you can return false it you want nothing to be put in the dom
      // This is also your chance to render a spinner or something...
      return <CircularProgress className="progressCircle"  />
    }


    return (
      <div>

        <h2 style={{ color: "#000000", marginLeft: '10%' }}>Temas disponibles: </h2>

        {(() => {
          if (tasks.length == 0) {
            return (
              <Card>
                <CardTitle>No hay tareas disponibles</CardTitle>
              </Card>
            )
          }
        })() }

        {
          tasks.map(function (task, i) {
            console.log(localStorage.subject)
            let show = true
            for (var j = 0; j < task.workflow.metadata.length; j++) {
              if (task.workflow.metadata[j].wfontology_Name === "pub_subject") {
                if (task.workflow.metadata[j].metadataValue === localStorage.subject) {
                  show = true
                } else {
                  show = false
                }
              }

            }
            console.log(show)
            let url = "/test"
            if (show) {
              return (

                <Card style={{ width: '80%', margin: 'auto' }} key ={i}>

                  <CardHeader
                    titleStyle={{ fontSize: '130%' }}
                    avatar={ <Avatar
                      backgroundColor={colors.blue400}
                      color ={colors.white}
                      >
                      {task.workflow.name[0]}
                    </Avatar>}

                    title={task.workflow.name}></CardHeader>
                  <CardActions>
                    {task.taskList.map(function (taskitem) {

                      return (

                        <RaisedButton
                          key ={taskitem.uri}
                          label ={taskitem.wfontology_Name}
                          primary={true}
                          onClick = {() => this.startTask(taskitem, task.executionId, task.caseId) }
                          ></RaisedButton>
                      );

                    }, this) }


                  </CardActions>

                  {(() => {
                    if (task.taskList.length == 0) {
                      return (
                        <CardText>No hay tareas disponibles de este tema para realizar ahora mismo</CardText>
                      )
                    }
                  })() }
                </Card>
              );
            }
          }, this) }
      </div>
    );
  }


}


function mapStateToProps(state) {
  return {

    tasks: state.TaskState.tasks,
    isLookingForTasks: state.TaskState.isLookingForTasks
  };
}

function mapDispatchToProps(dispatch) {
  return {
    TaskActions: bindActionCreators(TaskActions, dispatch),
    //Add here the binding for the different actions
    //      XXActions: bindActionCreators(XXActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
