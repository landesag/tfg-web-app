import React, { Component } from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, History } from 'react-router'
import PropTypes from 'react-router';
import Divider from 'material-ui/Divider';

import ThemeDecorator from 'material-ui/styles/themeDecorator';

import auth from '../auth'
import injectTapEventPlugin from 'react-tap-event-plugin';
import autobind from 'autobind-decorator'
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';
import GridList from 'material-ui/GridList/GridList';
import GridTile from 'material-ui/GridList/GridTile';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import LeftNav from 'material-ui/Drawer';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import CircularProgress from 'material-ui/CircularProgress';

import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import MenuItem from 'material-ui/MenuItem';
import Clear from 'material-ui/svg-icons/content/clear';

import * as UIActions from '../store/actions/UIActions'

import Dimensions from 'react-dimensions'

@Dimensions()
class ChooseSubject extends Component {

  //Se obtiene el router del contexto para las redirecciones automaticas
  static contextTypes = { router: React.PropTypes.object.isRequired };


  constructor(props, context) {
    super(props, context)
  }
  @autobind goHome(subjectId) {
    localStorage.setItem('subject', subjectId)

    this.context.router.push('/')
  }

  @autobind goHistory(subjectId) {
    localStorage.setItem('subject', subjectId)

    this.context.router.push('/history')
  }

  componentDidMount() {
    this.getSubjects()
  }

  @autobind addSubjectToStateSubjectsIfNotPresent(workflow) {
    var arrayLength = answer.optionsSelected.length;
    for (var i = 0; i < arrayLength; i++) {
      if (answer.optionsSelected[i].uri === option.uri) {
        optionWasAnswered = true;
      }
    }

  }

  @autobind getSubject(metadata) {
    let arrayLength = metadata.length;
    for (var i = 0; i < arrayLength; i++) {
      if (metadata[i].wfontology_Name === "pub_subject") {
        return metadata[i].metadataValue;
      }

    }

  }

  @autobind getSubjects(page = 0, subjects = []) {


    let token = localStorage.token
    $.ajax({
      url: "https://tec.citius.usc.es/cuestionarios/backend/HMBOntologyRESTAPI/api/client/v2/workflows/page/" + page + "/size/50",
      dataType: 'json',
      headers: {
        'X-Auth-Token': token
      },
      success: function (data) {
        console.log(data)
        let workflows = data.content.result;
        let arrayLength = workflows.length;
        for (var i = 0; i < arrayLength; i++) {
          let subject = this.getSubject(workflows[i].workflow.metadata)
          if (subjects.indexOf(subject) == -1) {
            subjects.push(subject)
          }
        }
        if (data.content.numberOfPages - 1 > page) {
          this.getSubject(page, subjects)
        } else {
          this.setState({
            subjectsState: subjects
          });
        }


      }.bind(this),
      error: function (xhr, status, err) {
      }.bind(this)
    });

  }

  state = {
    subjectsState: undefined,
    open: false
  }

  @autobind handleClose() {
    this.setState({
      open: false
    });
  };

  @autobind handleOpen() {
    this.setState({
      open: true
    });
  };

  @autobind exit() {
    auth.logout()
    this.context.router.push('/login')
  }



  render() {


    const actions = [
      <FlatButton
        label="SI"
        secondary={false}
        onClick={this.exit}
        />,
      <FlatButton
        label="NO"
        secondary={true}
        onClick={this.handleClose}
        />,
    ];


    if (this.state.subjectsState == undefined) {
      return (
        <CircularProgress className="progressCircle"/>
      )
    }

    return (


      <div className='app-container'>


        <Dialog
          title="¿Quieres salir ya?"
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
          >
          ¡Acabamos de empezar!
        </Dialog>

        <AppBar
          className = 'login-appbar-container'
          title = 'ge learn'
          iconElementLeft={<IconButton onTouchTap={ this.handleOpen }><Clear /></IconButton>}
          />


        <Card className = "wellcomeCard">


          <CardTitle title="¡Bienvenido!" subtitle="Selecciona la asignatura en la que vas a trabajar en este momento" />

        </Card>

        <div className= "subjectCardsDiv">

          {this.state.subjectsState.map(function (subject, i) {
            return (
              <Card key ={i} className="subjectCard">
                <CardMedia
                  overlay={<CardTitle title={subject} />}
                  >
                  <img src="../imgs/examplesubject.png"/>
                </CardMedia>
                <CardActions>
                  <FlatButton style={{ width: '45%' }}
                    onClick = {() => this.goHome(subject) }
                    >
                    Próximas tareas
                  </FlatButton>
                  <FlatButton style={{ width: '45%' }}
                    onClick = {() => this.goHistory(subject) }
                    >
                    Estadísticas
                  </FlatButton>
                </CardActions>
              </Card>
            )
          }, this) }

        </div>
      </div>

    )
  }

}

export default ChooseSubject
