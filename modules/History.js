import React, { Component, PropTypes } from 'react'
import { render } from 'react-dom'
import Dimensions from 'react-dimensions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import autobind from 'autobind-decorator'


import { Link } from 'react-router'

import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import FileFolder from 'material-ui/svg-icons/file/folder';
import styles from 'material-ui/styles';
import FontIcon from 'material-ui/FontIcon';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';

import * as colors from 'material-ui/styles/colors';
import HardwareKeyboardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';
import CardText from 'material-ui/Card/CardText';
import RaisedButton from 'material-ui/RaisedButton';
import Medal from './medalComponent/Medal'
import RatingContainer from './RatingContainer'
import Breadcrumbs from 'react-breadcrumbs'

import Done from 'material-ui/svg-icons/action/done';
import Clear from 'material-ui/svg-icons/content/clear';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';

@Dimensions()
class History extends Component {

  static contextTypes = { router: React.PropTypes.object.isRequired };

  //Se establece el estado del componente
  state = {
    history: undefined,
    subjectStats: undefined,
    unitStats: undefined,
    noStats: false
  }


  @autobind getSubjectsStats() {
    let token = localStorage.token
    let subject = encodeURIComponent(localStorage.subject)
    $.ajax({
      url: "https://tec.citius.usc.es/cuestionarios/backend/StatsRESTAPI/api/stats/v1/client/user/subject/" + subject,
      dataType: 'json',
      headers: {
        'X-Auth-Token': token
      },

      success: function (data) {
        console.log(data)
        if (data.content == undefined) {
          this.setState({
            noStats: true
          })
        }
        this.setState({
          subjectStats: data.content
        });
        this.getUnitsStats()
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(status)
      }.bind(this)
    });
  }



  @autobind getUnitsStats() {
    let token = localStorage.token
    let subject = encodeURIComponent(localStorage.subject)
    $.ajax({
      url: "https://tec.citius.usc.es/cuestionarios/backend/StatsRESTAPI/api/stats/v1/client/user/subject/" + subject + "/units",
      dataType: 'json',
      headers: {
        'X-Auth-Token': token
      },

      success: function (data) {
        console.log(data)

        this.setState({
          unitStats: data.content.result
        });
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(status)
      }.bind(this)
    });
  }

  componentDidMount() {
    this.getSubjectsStats()
  }

  constructor(props, context) {
    super(props, context)
  }

  findEvaluationOfQuestion(uri, evaluations) {

    var arrayLength = evaluations.length;
    for (var i = 0; i < arrayLength; i++) {
      if (evaluations[i].fullQuestionWithRating.uri == uri) {
        return evaluations[i];
      }
    }
  }

  timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
  }

  render() {
    let history = this.state.history
    let unitStats = this.state.unitStats
    let subjectStats = this.state.subjectStats
    console.log(history)

    if (this.state.noStats) {
      return (
        <div>
          <Breadcrumbs
            routes={this.props.routes}
            params={this.props.params}
            excludes={['App']}
            separator={<HardwareKeyboardArrowRight className="breadcrumbsIcon"/>}
            />

          <Card className="subjectStatsCard">
            <CardTitle title="Aún no hay estadísticas" />
          </Card>
        </div>
      )
    }
    if (!unitStats && !subjectStats) {
      // Note that you can return false it you want nothing to be put in the dom
      // This is also your chance to render a spinner or something...
      return <CircularProgress className="progressCircle"  />
    } else if (!subjectStats && unitStats != undefined) {
      return <CircularProgress className="progressCircle"  />

    } else if (!unitStats && subjectStats != undefined) {

      return (
        <div>
          <Card className="subjectStatsCard">
            <CardMedia
              overlay={<CardTitle title={subjectStats.subjectId} />}
              >
              <img src="../imgs/examplesubject.png" />
            </CardMedia>
            <CardTitle>

              <Medal lite = {false} rating = {subjectStats.decil}/>

              <br/>
              <RatingContainer max_rating={subjectStats.maxRating} rating ={subjectStats.rating}/>
            </CardTitle>
          </Card>

          <CircularProgress className="progressCircle"  />
        </div>


      )

    } else {

      let expandable = true
      if (window.innerWidth <= 800) {
        expandable = false

      }

      return (
        <div>

          <Breadcrumbs
            routes={this.props.routes}
            params={this.props.params}
            excludes={['App']}
            separator={<HardwareKeyboardArrowRight className="breadcrumbsIcon"/>}
            />

          <Card className="subjectStatsCard">
            <CardMedia
              overlay={<CardTitle title="Valoración en la asignatura" />}
              >
              <img src="../imgs/examplesubject.png" />
            </CardMedia>
            <CardTitle>

              <Medal lite = {false} rating = {subjectStats.decil}/>

              <br/>
              <RatingContainer max_rating={subjectStats.maxRating} rating ={subjectStats.rating}/>
            </CardTitle>
          </Card>

          {unitStats.map(function (unitStat, i) {
            let linkurl = "/history/" + unitStat.unitId
            return (
              <Card key={i} className="unitStatsCard">

                <CardHeader
                  title={unitStat.unitId}
                  titleStyle={{ fontSize: '110%' }}
                  avatar={
                    <Avatar>
                      <Medal lite = {true} rating = {unitStat.decil}/>
                    </Avatar>
                  }
                  actAsExpander={expandable}
                  showExpandableButton={expandable}>
                </CardHeader>
                <CardActions>
                  <Link to={linkurl}><RaisedButton className="buttonUnitStatsCard" label="Detalle cuestionarios" primary={true}/></Link>
                </CardActions>
                <CardText expandable={expandable}>

                  <div className="statsTab"> REALIZADOS<br/><div className="statsTabNumber">{unitStat.finishedQuestsCount}</div></div>
                  <div className="statsTab"> APROBADOS<br/><div className="statsTabNumber">{unitStat.passedQuestsCount}</div></div>
                  <div className="statsTab"> NO PRESENTADO<br/><div className="statsTabNumber">{unitStat.notPresentedQuestsCount}</div></div>
                  <div className="statsTab"> SUSPENSOS<br/><div className="statsTabNumber">{unitStat.failedQuestsCount}</div></div>

                </CardText>

              </Card>)
          }, this) }

        </div>

      );
    }
  }


}


export default History
