import React, { Component, PropTypes } from 'react'
import { render } from 'react-dom'
import Dimensions from 'react-dimensions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import autobind from 'autobind-decorator'


import { Link } from 'react-router'

import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import FileFolder from 'material-ui/svg-icons/file/folder';
import styles from 'material-ui/styles';
import FontIcon from 'material-ui/FontIcon';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';
import HardwareKeyboardArrowRight from 'material-ui/svg-icons/hardware/keyboard-arrow-right';

import * as colors from 'material-ui/styles/colors';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';
import CardText from 'material-ui/Card/CardText';
import RaisedButton from 'material-ui/RaisedButton';
import Medal from './medalComponent/Medal'
import RatingContainer from './RatingContainer'
import Breadcrumbs from 'react-breadcrumbs'

import Done from 'material-ui/svg-icons/action/done';
import Clear from 'material-ui/svg-icons/content/clear';
import FlatButton from 'material-ui/FlatButton';
import CircularProgress from 'material-ui/CircularProgress';

@Dimensions()
class UnitHistory extends Component {

  static contextTypes = { router: React.PropTypes.object.isRequired };

  //Se establece el estado del componente
  state = {
    history: undefined,
    subjectStats: undefined,
    unitStat: undefined,
    notFound: false
  }



  @autobind getSubjectsStats() {
    let token = localStorage.token
    let subject = encodeURIComponent(localStorage.subject)
    $.ajax({
      url: "https://tec.citius.usc.es/cuestionarios/backend/StatsRESTAPI/api/stats/v1/client/user/subject/" + subject,
      dataType: 'json',
      headers: {
        'X-Auth-Token': token
      },

      success: function (data) {
        console.log(data)

        this.setState({
          subjectStats: data.content
        });
        this.getUnitsStats()
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(status)
      }.bind(this)
    });
  }


  @autobind compareTimestampHistoryEntries(h1, h2) {
    if (h1.timestamp < h2.timestamp) {
      return -1
    }
    if (h1.timestamp > h2.timestamp) {
      return 1
    }
    return 0
  }

  @autobind getQuestsStats() {
    let token = localStorage.token
    let unitId = encodeURIComponent(this.props.params.unitID)
    let subject = encodeURIComponent(localStorage.subject)

    $.ajax({
      url: "https://tec.citius.usc.es/cuestionarios/backend/StatsRESTAPI/api/stats/v1/client/user/subject/" + subject + "/unit/" + unitId + "/quests",
      dataType: 'json',
      headers: {
        'X-Auth-Token': token
      },

      success: function (data) {
        console.log(data)
        let his = data.content.result
        his.sort(this.compareTimestampHistoryEntries)
        this.setState({
          history: his
        });
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(status)
      }.bind(this)
    });
  }

  @autobind getUnitStats() {
    let token = localStorage.token
    let unitId = encodeURIComponent(this.props.params.unitID)
    let subject = encodeURIComponent(localStorage.subject)

    $.ajax({
      url: "https://tec.citius.usc.es/cuestionarios/backend/StatsRESTAPI/api/stats/v1/client/user/subject/" + subject + "/unit/" + unitId,
      dataType: 'json',
      headers: {
        'X-Auth-Token': token
      },

      success: function (data) {
        console.log(data)
        if (data.content != undefined) {
          this.setState({
            unitStat: data.content
          });
          this.getQuestsStats()

        } else {
          this.setState({
            notFound: true
          });
        }
      }.bind(this),
      error: function (xhr, status, err) {
        console.log(status)
      }.bind(this)
    });
  }

  componentDidMount() {
    this.getUnitStats()
  }

  constructor(props, context) {
    super(props, context)
  }

  findEvaluationOfQuestion(uri, evaluations) {

    var arrayLength = evaluations.length;
    for (var i = 0; i < arrayLength; i++) {
      if (evaluations[i].fullQuestionWithRating.uri == uri) {
        return evaluations[i];
      }
    }
  }

  timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
  }

  render() {
    let history = this.state.history
    let unitStat = this.state.unitStat
    let subjectStats = this.state.subjectStats
    console.log(history)

    if (this.state.notFound) {
      return <h1>Error 404: El tema indicado no existe :(</h1>
    }
    if (!unitStat || !history) {
      // Note that you can return false it you want nothing to be put in the dom
      // This is also your chance to render a spinner or something...
      return <CircularProgress className="progressCircle"  />
    } else {


      return (

        <div>


          <Breadcrumbs
            routes={this.props.routes}
            params={this.props.params}
            excludes={['App']}
            separator={<HardwareKeyboardArrowRight className="breadcrumbsIcon"/>}
            />

          <Card className="unitCard">
            <CardTitle style={{ fontSize: '150%' }}>{unitStat.unitId}

            </CardTitle>

            <Medal lite = {false} rating = {unitStat.decil}/>

            <br/>
            <RatingContainer max_rating={unitStat.maxRating} rating ={unitStat.rating}/>
            <CardText>

              <div className="statsTab"> REALIZADOS<br/><div className="statsTabNumber">{unitStat.finishedQuestsCount}</div></div>
              <div className="statsTab"> APROBADOS<br/><div className="statsTabNumber">{unitStat.passedQuestsCount}</div></div>
              <div className="statsTab"> NO PRESENTADO<br/><div className="statsTabNumber">{unitStat.notPresentedQuestsCount}</div></div>
              <div className="statsTab"> SUSPENSOS<br/><div className="statsTabNumber">{unitStat.failedQuestsCount}</div></div>

            </CardText>


          </Card>


          <div>
            {history.map(function (historyEntry, i) {

              let time = this.timeConverter(historyEntry.timestamp)

              let title = historyEntry.evaluation.fullQuestionnaire.questionnaires_Name + " " + historyEntry.evaluation.rating + "/" + historyEntry.evaluation.fullQuestionnaire.maxRating;
              return (
                <div key={i}>


                  <Card className="historyCard">

                    <CardHeader
                      title={title}
                      subtitle={time}
                      actAsExpander={true}
                      showExpandableButton={true}
                      >
                    </CardHeader>


                    {
                      historyEntry.evaluation.fullQuestionnaire.fullQuestions.map(function (fullQuestionWithRating, j) {

                        let evaluation = this.findEvaluationOfQuestion(fullQuestionWithRating.uri, historyEntry.evaluation.questionsEvaluation)
                        let answer = undefined;
                        let ratingOfUser = 0
                        let notSwitch = false;
                        if (evaluation != undefined) {
                          answer = evaluation.answer
                          ratingOfUser = evaluation.rating
                        } else {
                        }
                        let fullQuestion = fullQuestionWithRating.fullQuestion
                        if (!notSwitch) {

                          switch (fullQuestion['@class']) {


                            //Pregunta de elegir varias opciones opcion
                            case "es.usc.citius.hmb.questionnaires.model.ChooseOneOptionFullQuestion":

                              return (
                                <CardActions key={j} expandable={true}>
                                  <Divider/>

                                  <CardText>
                                    {j + 1} {fullQuestion.title}
                                  </CardText>



                                  {fullQuestion.fullOptions.map(function (option, j) {

                                    let optionWasAnswered = false;
                                    if (answer != undefined) {
                                      if (answer.optionSelected.uri === option.uri) {
                                        optionWasAnswered = true;

                                      }
                                    }
                                    let icon = undefined;
                                    if (optionWasAnswered) {
                                      if (option.isCorrect == true) {
                                        icon = <Done />
                                      } else {
                                        icon = <Clear />
                                      }

                                    }

                                    return (
                                      <FlatButton
                                        key={j}
                                        icon ={icon}
                                        primary = {option.isCorrect}
                                        label={option.questionnaires_Value.stringValue}

                                        />);

                                  }
                                    , this) }

                                  <div className = 'rating'>
                                    {ratingOfUser}/{fullQuestionWithRating.maxRating}
                                  </div>

                                </CardActions>


                              );


                            //Pregunta de verdadero y falso
                            case "es.usc.citius.hmb.questionnaires.model.TrueFalseFullQuestion":
                              let icon = undefined;
                              let icon2 = undefined;
                              if (answer != undefined) {
                                if (answer.optionSelected.questionnaires_Value.booleanValue == true) {
                                  if (fullQuestion.fullOptions[0].isCorrect) {
                                    icon = <Done />
                                  } else {
                                    icon = <Clear />
                                  }
                                } else {
                                  if (fullQuestion.fullOptions[1].isCorrect) {
                                    icon2 = <Done />
                                  } else {
                                    icon2 = <Clear />
                                  }
                                }
                              }
                              return (
                                <CardActions key={j} expandable={true}>
                                  <Divider/>

                                  <CardText>
                                    {j + 1} {fullQuestion.title}
                                  </CardText>


                                  <FlatButton icon={icon} primary = {fullQuestion.fullOptions[0].isCorrect} label="Verdadero"/>
                                  <FlatButton icon ={icon2} primary = {fullQuestion.fullOptions[1].isCorrect} label="Falso"/>
                                  <div className = 'rating'>
                                    {ratingOfUser}/{fullQuestionWithRating.maxRating}
                                  </div>

                                </CardActions>


                              );


                            //Pregunta de elegir varias opciones opcion
                            case "es.usc.citius.hmb.questionnaires.model.ChooseVariousOptionsFullQuestion":


                              return (
                                <CardActions key={j} expandable={true}>
                                  <Divider/>

                                  <CardText>
                                    {j + 1} {fullQuestion.title}
                                  </CardText>



                                  {fullQuestion.fullOptions.map(function (option, j) {

                                    let optionWasAnswered = false;
                                    if (answer != undefined) {
                                      var arrayLength = answer.optionsSelected.length;
                                      for (var i = 0; i < arrayLength; i++) {
                                        if (answer.optionsSelected[i].uri === option.uri) {
                                          optionWasAnswered = true;
                                        }
                                      }
                                    }
                                    let icon = undefined;
                                    if (optionWasAnswered) {
                                      if (option.isCorrect == true) {
                                        icon = <Done />
                                      } else {
                                        icon = <Clear />
                                      }

                                    }

                                    return (
                                      <FlatButton
                                        key={j}
                                        icon ={icon}
                                        primary = {option.isCorrect}
                                        label={option.questionnaires_Value.stringValue}

                                        />);

                                  }
                                    , this) }

                                  <div className = 'rating'>
                                    {ratingOfUser}/{fullQuestionWithRating.maxRating}
                                  </div>

                                </CardActions>


                              );

                            case "es.usc.citius.hmb.questionnaires.model.InsertOneTextFullQuestion":


                              var text = [];
                              var pattext = "<_.*_>";
                              var patt = new RegExp(pattext);

                              var str = fullQuestion.description.split(" ");

                              for (var i = 0; i < str.length; i++) {

                                if (patt.test(str[i])) {

                                  let uripre = str[i].split("<_")[1]
                                  let uri = uripre.split("_>")[0]

                                  if (evaluation == undefined) {
                                    text.push(<span className="rightSpan" key={i}>{fullQuestionWithRating.fullQuestion.fullOption.questionnaires_Value.stringValue}</span>);

                                  } else if (evaluation.fullQuestionWithRating.fullQuestion.fullOption.isCorrect) {
                                    text.push(<Done  key={i + 100}/>)
                                    text.push(<span className = "rightSpan" key={i}>{evaluation.fullQuestionWithRating.fullQuestion.fullOption.questionnaires_Value.stringValue}</span>);
                                  } else {

                                    text.push(<Clear key={i + 100} />)
                                    text.push(<span className="rightSpan" key={i}>{evaluation.fullQuestionWithRating.fullQuestion.fullOption.questionnaires_Value.stringValue}</span>);
                                  }

                                } else {
                                  text.push(<span key={i}>{str[i]}</span>)
                                }
                              }


                              return (

                                <CardActions key={j} expandable={true}>
                                  <Divider/>

                                  <CardText>
                                    {j + 1} {fullQuestion.title}
                                  </CardText>



                                  {text}

                                  , this) }

                                  <div className = 'rating'>

                                    {ratingOfUser}/{fullQuestionWithRating.maxRating}

                                  </div>

                                </CardActions>
                              );


                            case "es.usc.citius.hmb.questionnaires.model.InsertVariousTextsFullQuestion":
                              console.log('lol')

                              var text = [];
                              var pattext = "<_.*_>";
                              var patt = new RegExp(pattext);

                              var str = fullQuestion.description.split(" ");
                              console.log(fullQuestion.description)
                              console.log(str)

                              for (var i = 0; i < str.length; i++) {
                                console.log(i)
                                if (patt.test(str[i])) {

                                  let uripre = str[i].split("<_")[1]
                                  let uri = uripre.split("_>")[0]
                                  let rightAnswer = undefined;
                                  let answerUser = undefined;
                                  //Obtener la respuesta correcta del cuestionario completo
                                  for (var k = 0; k < fullQuestionWithRating.fullQuestion.fullOptions.length; k++) {
                                    if (fullQuestionWithRating.fullQuestion.fullOptions[k].uri === uri) {
                                      rightAnswer = fullQuestionWithRating.fullQuestion.fullOptions[k].questionnaires_Value.stringValue;
                                    }
                                  }

                                  if (answer != undefined) {
                                    //Obtener la respuesta del usuario de las respuestas
                                    for (var k = 0; k < answer.optionsSelected.length; k++) {
                                      if (answer.optionsSelected[k].uri === uri) {
                                        answerUser = answer.optionsSelected[k].questionnaires_Value.stringValue;
                                      }
                                    }
                                  } else {
                                    answerUser = " "
                                  }
                                  if (rightAnswer === answerUser) {
                                    text.push(<span className = "rightSpan" key={i}><Done key={i + 100}/>{answerUser}</span>);
                                  } else {
                                    if (evaluation == undefined) {
                                      text.push(<span className="rightSpan" key={i}>{rightAnswer}</span>);
                                    } else {
                                      text.push(<Clear key={i + 100} />)
                                      text.push(<span className="rightSpan" key={i}>{answerUser}</span>);
                                    }
                                  }

                                } else {
                                  text.push(<span key={i}>{str[i]}</span>)
                                }
                              }


                              return (

                                <CardActions key={j} expandable={true}>
                                  <Divider/>

                                  <CardText>
                                    {j + 1} {fullQuestion.title}
                                  </CardText>



                                  {text}

                                  , this) }

                                  <div className = 'rating'>

                                    {ratingOfUser}/{fullQuestionWithRating.maxRating}

                                  </div>

                                </CardActions>
                              );


                          }
                        }
                      }, this)
                    }

                  </Card>
                </div>
              );

            }, this) }
          </div>


        </div >
      );
    }
  }


}


export default UnitHistory
