import React, { Component, PropTypes } from 'react'


import Dimensions from 'react-dimensions'

@Dimensions()
class Medal extends Component {


  constructor(props, context) {
    super(props, context)
  }



  render() {
    let width = "140"
    let height = "90"
    if (this.props.lite) {
      width = "40";
      height = "40";
    }
    switch (this.props.rating) {
      case 0:
        return (

          <img src="../imgs/medals/medal_0.png" height={height} width={height}/>
        )
      case 1:
        return (

          <img src="../imgs/medals/medal_1.png" height={height} width={height}/>
        )
      case 2:
        return (

          <img src="../imgs/medals/medal_2.png" height={height} width={height}/>
        )
      case 3:
        return (

          <img src="../imgs/medals/medal_3.png" height={height} width={height}/>
        )
      case 4:
        return (

          <img src="../imgs/medals/medal_4.png" height={height} width={height}/>
        )
      case 5:
        return (

          <img src="../imgs/medals/medal_5.png" height={height} width={height}/>
        )
      case 6:
        return (

          <img src="../imgs/medals/medal_6.png" height={height} width={height}/>
        )
      case 7:
        if (this.props.lite) {
          return (

            <img src="../imgs/medals/medal_7_lite.png" height={height} width={width}/>
          )
        }
        return (

          <img src="../imgs/medals/medal_7.png" height={height} width={width}/>
        )
      case 8:
        if (this.props.lite) {
          return (

            <img src="../imgs/medals/medal_8_lite.png" height={height} width={width}/>
          )
        }
        return (

          <img src="../imgs/medals/medal_8.png" height={height} width={width}/>
        )
      case 9:
        if (this.props.lite) {
          return (

            <img src="../imgs/medals/medal_9_lite.png" height={height} width={width}/>
          )
        }
        return (

          <img src="../imgs/medals/medal_9.png" height={height} width={width}/>
        )
      case 10:
        if (this.props.lite) {
          return (

            <img src="../imgs/medals/medal_10_lite.png" height={height} width={width}/>
          )
        }
        return (

          <img src="../imgs/medals/medal_10.png" height={height} width={width}/>
        )


      default:
        return (

          <img src="../imgs/medals/medal_8.png" height={height} width={width}/>
        )

    }

  }


  static propTypes = {
    rating: PropTypes.number.isRequired,
    lite: PropTypes.bool.isRequired
  };

}

export default Medal