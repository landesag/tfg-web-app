import React, { Component } from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, History } from 'react-router'
import PropTypes from 'react-router';

import ThemeDecorator from 'material-ui/styles/themeDecorator';

import auth from '../auth'
import injectTapEventPlugin from 'react-tap-event-plugin';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';

import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import LeftNav from 'material-ui/Drawer';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import MenuItem from 'material-ui/MenuItem';

import * as UIActions from '../store/actions/UIActions'

import Dimensions from 'react-dimensions'

@Dimensions()
class Login extends Component {

  //Se obtiene el router del contexto para las redirecciones automaticas
  static contextTypes = { router: React.PropTypes.object.isRequired };



  constructor(props, context) {
    super(props, context)
  }

  //Se establece el estado del componente
  state = {
    open: false, //Se pondrá a true cuando haya error en el login y se abra el popup
    EmailValue: '',
    PassValue: ''

  }



  _handleEmailFieldChange = (e) => {
    this.setState({
      EmailValue: e.target.value
    });
  };

  _handlePassFieldChange = (e) => {
    this.setState({
      PassValue: e.target.value
    });
  };

  handleSubmit = (event) => {
    event.preventDefault()

    const email = this.state.EmailValue
    const pass = this.state.PassValue

    auth.login(email, pass, (loggedIn) => {
      if (!loggedIn) {
        this.handleOpen()
        return
      }

      const { location } = this.props

      if (location.state && location.state.nextPathname) {
        this.context.router.push(location.state.nextPathname)
      } else {
        this.context.router.push('/')
      }
    })
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  handleOpen = () => {
    this.setState({
      open: true
    });
  };


  render() {

    const actions = [
      <FlatButton
        label="OK"
        secondary={true}
        onClick={this.handleClose}
        />,
    ];


    return (


      <div className='app-container'>


        <AppBar
          className = 'login-appbar-container'
          title = 'ge learn'
          showMenuIconButton = { false }
          />


        <Card className='login-card'>


          <CardTitle title="Login" subtitle="Inicia sesion con tu cuenta" />
          <img className = "imgLogin" src="../imgs/logo-student-project.png"></img>

          <CardActions className ='login-card-actions'>
            <form action={this.handleSubmit}>
              <TextField hintText="Usuario" value={this.state.EmailValue} onChange={this._handleEmailFieldChange}/><br/>
              <TextField type = "password" hintText="Contraseña" value={this.state.PassValue} onChange={this._handlePassFieldChange} /><br/>


              <br/>
              <RaisedButton type="submit" label="Iniciar sesión" onClick={this.handleSubmit}/>

            </form>

          </CardActions>

          <Dialog
            title="Error de inicio de sesion"
            actions={actions}
            modal={false}
            open={this.state.open}
            onRequestClose={this.handleClose}
            >
            El usuario y contraseña introducidos no coniciden con uno válido.
          </Dialog>

        </Card>



      </div>

    )
  }

}

export default Login
