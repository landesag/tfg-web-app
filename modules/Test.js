import React, { Component, PropTypes } from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, History } from 'react-router'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import ThemeDecorator from 'material-ui/styles/themeDecorator';
import LinearProgress from 'material-ui/LinearProgress';

import SingleAnswerQuestion from './questions/SingleAnswerQuestion'
import TrueFalseQuestion from './questions/TrueFalseQuestion'
import ChooseVariousOptionsQuestion from './questions/ChooseVariousOptionsQuestion'
import TextQuestion from './questions/TextQuestion'
import VariousTextQuestion from './questions/VariousTextQuestion'

import auth from '../auth'
import injectTapEventPlugin from 'react-tap-event-plugin';
import Snackbar from 'material-ui/Snackbar';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';
import Avatar from 'material-ui/Avatar';

import styles from 'material-ui/styles';
import FontIcon from 'material-ui/FontIcon';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';

import * as colors from 'material-ui/styles/colors';

import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import LeftNav from 'material-ui/Drawer';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Divider from 'material-ui/Divider';

import Slider  from 'react-slick'
import ArrowBack from 'material-ui/svg-icons/navigation/arrow-back';
import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import MenuItem from 'material-ui/MenuItem';

import * as UIActions from '../store/actions/UIActions'

import * as MODEL from './model'
import Dimensions from 'react-dimensions'

import autobind from 'autobind-decorator'

let slider;

@Dimensions()
class Test extends Component {

  //Se obtiene el router del contexto para las redirecciones automaticas
  static contextTypes = { router: React.PropTypes.object.isRequired };



  constructor(props, context) {
    super(props, context)

    this.goBack = this.goBack.bind(this)

  }


  @autobind stateContainsAnswerOfQuestion(questionAnswer) {
    var answersvar = this.state.questionanswers.slice()

    var arrayLength = answersvar.length;
    for (var i = 0; i < arrayLength; i++) {
      if (questionAnswer.questionId === answersvar[i].questionId) {
        return i
      }
    }
    return -1
  }
  @autobind addQuestionAnswer(questionAnswer) {
    console.log(questionAnswer)
    let index = this.stateContainsAnswerOfQuestion(questionAnswer)
    if (index == -1) {

      var answersvar = this.state.questionanswers.slice()
      answersvar.push(questionAnswer)
      this.setState({ questionanswers: answersvar })

    } else {
      var answersvar = this.state.questionanswers.slice()
      answersvar[index] = questionAnswer
      this.setState({ questionanswers: answersvar })

    }
    console.log(this.state.questionanswers)

  }

  componentDidMount() {
    console.log(this.state.task)
  }

  @autobind handleSubmit() {
    if (this.state.questionanswers.length < this.state.task.parameterValue[0].namedParameterValue.questions.length) {
      this.handleOpen()
    } else {
      this.finishForm()
    }
  }

  @autobind finishForm() {

    let questionnaireAnswersObj = {
      answers: this.state.questionanswers,
      questionnaireId: this.state.task.parameterValue[0].namedParameterValue.uri
    }

    let questionnaireAnswer = new MODEL.QuestionnaireAnswer(questionnaireAnswersObj)
    questionnaireAnswer.genURI();
    console.log(JSON.stringify(questionnaireAnswer))

    let token = localStorage.token

    //Se envian las ultimas respuestas introducidas por el usuario
    let urlsend = "https://tec.citius.usc.es/cuestionarios/backend/HMBWFExecutionEngineRESTAPI/api/client/v2/execution/"
      + localStorage.executionId + "/case/" + localStorage.caseId + "/localcaseproperty/quest_answer"

    let datasend = "value=" + encodeURIComponent(JSON.stringify(questionnaireAnswer))
    $.ajax({
      url: urlsend,
      type: "PUT",
      data: datasend,
      contentType: "application/x-www-form-urlencoded",
      headers: {
        'X-Auth-Token': token
      },
      success: function (data) {
        console.log('Resultados almacenados correctamente en el servidor.')


        //Se confirma la finalización del test
        urlsend = "https://tec.citius.usc.es/cuestionarios/backend/HMBWFExecutionEngineRESTAPI/api/client/v2/execution/"
          + localStorage.executionId + "/case/" + localStorage.caseId + "/localcaseproperty/quest_ok"

        datasend = "value=true"
        $.ajax({
          url: urlsend,
          type: "PUT",
          data: datasend,
          contentType: "application/x-www-form-urlencoded",
          headers: {
            'X-Auth-Token': token
          },
          success: function (data) {
            console.log('Test finalizado correctamente.')
            localStorage.removeItem('task')

            this.goBackEnd();

          }.bind(this),
          error: function (xhr, status, err) {
            console.log(status)
            localStorage.removeItem('task')

            this.handleClose()
            this.handleOpenError()
          }.bind(this)
        });



      }.bind(this),
      error: function (xhr, status, err) {
        console.log(status)
        localStorage.removeItem('task')

        this.handleClose()
        this.handleOpenError()
      }.bind(this)
    });


  }

  state = {
    task: JSON.parse(localStorage.getItem('task')),
    questionanswers: [],
    openDialog: false,
    openDialogError: false,
    openDialogNoEntregado: false,
    openDialogFinalizar: false,
    openSnackbar: false
  }


  @autobind handleCloseError() {
    this.setState({
      openDialogError: false
    });
    this.context.router.push('/')
  };

  @autobind handleCloseNoEntregado() {
    this.setState({
      openDialogNoEntregado: false
    });
  };

  @autobind handleOpenNoEntregado() {
    this.setState({
      openDialogNoEntregado: true
    });
  };

  @autobind handleCloseFinalizar() {
    this.setState({
      openDialogFinalizar: false
    });
  };

  @autobind handleOpenFinalizar() {
    this.setState({
      openDialogFinalizar: true
    });
  };


  //FUNCION PARA MOSTRAR EL TOAST DE TAREA NO ENTREGADA
  @autobind openSnackNoEntregada() {
    this.props.UIActions.openSnack("Tarea no entregada")
  }

  @autobind openSnackEntregada() {
    this.props.UIActions.openSnack("Tarea entregada")
  }


  @autobind onRequestCloseSnackbar() {
    this.setState({
      openSnackbar: false
    });
  }

  @autobind onRequestOpenSnackbar() {
    this.setState({
      openSnackbar: true
    });
  }
  @autobind handleOpenError() {
    this.setState({
      openDialogError: true
    });
  };


  @autobind handleClose() {
    this.setState({
      openDialog: false
    });

  };

  @autobind handleOpen() {
    this.setState({
      openDialog: true
    });
  };

  goBack(event) {
    this.openSnackNoEntregada()

    this.context.router.push('/')
  }

  goBackEnd(event) {
    this.openSnackEntregada()

    this.context.router.push('/')
  }

  render() {


    const actions = [
      <FlatButton
        label="Sí"
        primary={true}
        onClick={this.finishForm}
        />,
      <FlatButton
        label="No"
        secondary={true}
        onClick={this.handleClose}
        />
    ];


    const actionsNoEntregado = [
      <FlatButton
        label="Sí"
        primary={true}
        onClick={this.goBack}
        />,
      <FlatButton
        label="No"
        secondary={true}
        onClick={this.handleCloseNoEntregado}
        />
    ];


    const actionsFinalizar = [
      <FlatButton
        label="Sí"
        primary={true}
        onClick={this.finishForm}
        />,
      <FlatButton
        label="No"
        secondary={true}
        onClick={this.handleCloseFinalizar}
        />
    ];

    const actionsError = [
      <FlatButton
        label="Salir del cuestionario"
        secondary={true}
        onClick={this.handleCloseError}
        />
    ];

    var settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      arrows: false,
      className: 'slides',
      slidesToScroll: 1,
      centerMode: true

    };

    return (


      <div className='game-container'>

        <AppBar
          className = 'login-appbar-container'
          title = {this.state.task.wfontology_Name}
          showMenuIconButton = { true }
          iconElementLeft={<IconButton onTouchTap={ this.handleOpenNoEntregado }><ArrowBack /></IconButton>}
          />
        <div className='main-container'>


          <Dialog
            title="Faltan preguntas por contestar"
            actions={actions}
            modal={false}
            open={this.state.openDialog}
            onRequestClose={this.handleClose}
            >
            ¿Seguro que quieres finalizar el cuestionario?
          </Dialog>

          <Dialog
            title="¿Finalizar el cuestionario?"
            actions={actionsFinalizar}
            modal={false}
            open={this.state.openDialogFinalizar}
            onRequestClose={this.handleCloseFinalizar}
            >
            No podrás volver a cambiar tus respuestas
          </Dialog>


          <Dialog
            title="No has entregado"
            actions={actionsNoEntregado}
            modal={false}
            open={this.state.openDialogNoEntregado}
            onRequestClose={this.handleCloseNoEntregado}
            >
            ¿Seguro que quieres salir sin entregar?
          </Dialog>

          <Dialog
            title="Parece que hubo un error al finalizar el cuestionario"
            actions={actionsError}
            modal={false}
            open={this.state.openDialogError}
            onRequestClose={this.handleCloseError}
            >
            La tarea ya no esta disponible
          </Dialog>

          <h2>{this.state.task.operator.wfontology_Name}</h2>
          <h3>{this.state.task.operator.description}</h3>

          <LinearProgress mode="determinate" max = {this.state.task.parameterValue[0].namedParameterValue.questions.length}
            value={this.state.questionanswers.length} />
          <br/>

          {(() => {
            if (window.innerWidth <= 800) {
              return (


                <Slider  {...settings}>


                  {this.state.task.parameterValue[0].namedParameterValue.questions.map(function (question, i) {
                    let type = question.question['@class']

                    //Test de una sola respuesta correcta
                    switch (type) {

                      case "es.usc.citius.hmb.questionnaires.model.InsertOneTextQuestion":
                        return (
                          <div key={i}>
                            <Avatar className='numberAvatar' backgroundColor={colors.blue500} color ={colors.white}>{i + 1}</Avatar>

                            <TextQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>
                            <br/>
                            <br/>

                          </div>
                        );


                      case "es.usc.citius.hmb.questionnaires.model.InsertVariousTextsQuestion":
                        return (
                          <div key={i}>
                            <Avatar className='numberAvatar' backgroundColor={colors.blue500} color ={colors.white}>{i + 1}</Avatar>

                            <VariousTextQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>

                            <br/>
                            <br/>

                          </div>
                        );

                      case "es.usc.citius.hmb.questionnaires.model.TrueFalseQuestion":

                        return (
                          <div key={i}>
                            <Avatar className='numberAvatar' backgroundColor={colors.blue500} color ={colors.white}>{i + 1}</Avatar>

                            <TrueFalseQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>

                            <br/>
                            <br/>

                          </div>
                        );


                      case "es.usc.citius.hmb.questionnaires.model.ChooseVariousOptionsQuestion":
                        return (
                          <div key={i}>
                            <Avatar className='numberAvatar' backgroundColor={colors.blue500} color ={colors.white}>{i + 1}</Avatar>

                            <ChooseVariousOptionsQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>

                            <br/>
                            <br/>

                          </div>
                        );

                      default:

                        return (
                          <div key={i}>
                            <Avatar className='numberAvatar' backgroundColor={colors.blue500} color ={colors.white}>{i + 1}</Avatar>

                            <SingleAnswerQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>

                            <br/>
                            <br/>

                          </div>
                        );

                    }

                  }, this) }


                </Slider>

              )
            } else {
              return (
                <div >


                  {this.state.task.parameterValue[0].namedParameterValue.questions.map(function (question, i) {
                    let type = question.question['@class']

                    //Test de una sola respuesta correcta
                    switch (type) {

                      case "es.usc.citius.hmb.questionnaires.model.InsertOneTextQuestion":
                        return (
                          <div key={i}>

                            <TextQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>
                            <br/>
                            <br/>

                          </div>
                        );


                      case "es.usc.citius.hmb.questionnaires.model.InsertVariousTextsQuestion":
                        return (
                          <div key={i}>

                            <VariousTextQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>

                            <br/>
                            <br/>

                          </div>
                        );

                      case "es.usc.citius.hmb.questionnaires.model.TrueFalseQuestion":

                        return (
                          <div key={i}>

                            <TrueFalseQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>

                            <br/>
                            <br/>

                          </div>
                        );


                      case "es.usc.citius.hmb.questionnaires.model.ChooseVariousOptionsQuestion":
                        return (
                          <div key={i}>

                            <ChooseVariousOptionsQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>

                            <br/>
                            <br/>

                          </div>
                        );

                      default:

                        return (
                          <div key={i}>

                            <SingleAnswerQuestion addQuestionAnswer = {this.addQuestionAnswer} question = {question.question}/>

                            <br/>
                            <br/>

                          </div>
                        );

                    }

                  }, this) }

                </div>)
            }
          })() }



          <RaisedButton primary={true} label="Finalizar Test" fullWidth={true} onClick={this.handleOpenFinalizar}/>

        </div>

      </div >

    )
  }

  static propTypes = {

    isSnackbarOpened: PropTypes.bool.isRequired,
    snackbarMessage: PropTypes.string.isRequired,
  };
}


function mapStateToProps(state) {
  return {
    isSnackbarOpened: state.UIState.isSnackbarOpened,
    snackbarMessage: state.UIState.snackbarMessage
  };
}

function mapDispatchToProps(dispatch) {
  return {
    UIActions: bindActionCreators(UIActions, dispatch),
    //Add here the binding for the different actions
    //      XXActions: bindActionCreators(XXActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Test);




