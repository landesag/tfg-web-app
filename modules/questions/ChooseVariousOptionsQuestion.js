import React, { Component } from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, History } from 'react-router'
import PropTypes from 'react-router';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


import ThemeDecorator from 'material-ui/styles/themeDecorator';

import injectTapEventPlugin from 'react-tap-event-plugin';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';

import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import LeftNav from 'material-ui/Drawer';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import Checkbox from 'material-ui/Checkbox';


import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import MenuItem from 'material-ui/MenuItem';

import * as UIActions from '../../store/actions/UIActions'
import autobind from 'autobind-decorator'


import RadioButton from 'material-ui/RadioButton';
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup';

import * as MODEL from '../model'


class ChooseVariousOptionsQuestion extends Component {


  constructor(props, context) {
    super(props, context)
    this.changeActualAnswer = this.changeActualAnswer.bind(this);
    this.addOption = this.addOption.bind(this);
    this.removeOption = this.removeOption.bind(this);
    this.state = {
      optionsNow: [],
      buttonsMap: {}
    }

  }



  componentDidMount() {
    console.log(this.props.question)

    var map = new Object();


    var arrayLength = this.props.question.options.length;
    for (var i = 0; i < arrayLength; i++) {
      map[i] = false;
    }
    this.setState({ buttonsMap: map });

  }

  addOption(options, option) {

    var arrayLength = options.length;
    for (var i = 0; i < arrayLength; i++) {
      if (option.uri === options[i].uri) {
        return;
      }
    }

    options.push(option)

  }

  removeOption(options, option) {

    var arrayLength = options.length;
    for (var i = 0; i < arrayLength; i++) {
      if (option.uri === options[i].uri) {

        options.splice(i, 1);
        return;
      }
    }

  }


  changeActualAnswer(event, value, index) {

    let mustAdd = !this.state.buttonsMap[index];
    let map = this.state.buttonsMap;
    let options = this.state.optionsNow;
    if (mustAdd) {
      this.addOption(options, value)
      this.setState({ optionsNow: options })

      map[index] = true;
      this.setState({ buttonsMap: map });
    } else {
      this.removeOption(options, value)
      this.setState({ optionsNow: options })

      map[index] = false;
      this.setState({ buttonsMap: map });
    }


    this.setState

    let questionAnswerObj = {
      optionsSelected: this.state.optionsNow,
      questionId: this.props.question.uri
    }



    let questionAnswer = new MODEL.ChooseVariousOptionsQuestionAnswer(questionAnswerObj)
    questionAnswer.genURI()


    this.props.addQuestionAnswer(questionAnswer)


  }


  render() {

    let question = this.props.question

    return (

      <Card className='game-card'>


        <CardTitle title={question.title} id={question.uri}/>

        <CardActions>



          {question.options.map(function (option, j) {

            return (
              <RaisedButton
                style={{ marginBottom: '5px' }}
                key={j}
                primary = {this.state.buttonsMap[j]}
                label={option.questionnaires_Value.stringValue}
                onClick={(evt) => this.changeActualAnswer(evt, option, j) }
                />);
          }, this) }


          <br/>



        </CardActions>

      </Card>
    )
  }
}

export default ChooseVariousOptionsQuestion
