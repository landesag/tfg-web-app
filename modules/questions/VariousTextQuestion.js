import React, { Component } from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, History } from 'react-router'
import PropTypes from 'react-router';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


import ThemeDecorator from 'material-ui/styles/themeDecorator';

import injectTapEventPlugin from 'react-tap-event-plugin';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';

import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import LeftNav from 'material-ui/Drawer';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import MenuItem from 'material-ui/MenuItem';

import * as UIActions from '../../store/actions/UIActions'
import autobind from 'autobind-decorator'


import RadioButton from 'material-ui/RadioButton';
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup';

import * as MODEL from '../model'


class VariousTextQuestion extends Component {


  constructor(props, context) {
    super(props, context)
    this.state = {
      answersMap: {},
      values: {},
      text: []
    }
  }


  componentDidMount() {
    console.log(this.props.question)
    this.handleIntro = this.handleIntro.bind(this);
    this.handleAdd = this.handleAdd.bind(this);



    var map = new Object();


    var arrayLength = this.props.question.options.length;
    for (var i = 0; i < arrayLength; i++) {
      map[this.props.question.options[i].uri] = this.props.question.options[i];

      let stringObj = {
        stringValue: ""
      }

      let stringvalue = new MODEL.StringType(stringObj)
      stringvalue.genURI()

      map[this.props.question.options[i].uri].questionnaires_Value = stringvalue

    }
    this.setState({ answersMap: map });

    var text = [];
    var pattext = "<_.*_>";
    var patt = new RegExp(pattext);

    var str = this.props.question.description.split(" ");
    for (var i = 0; i < str.length; i++) {

      if (patt.test(str[i])) {
        let uripre = str[i].split("<_")[1]
        let uri = uripre.split("_>")[0]
        text.push(<TextField key={i} id={uri} name = "ResponseTextField" value={this.state.values[uri]} onChange={(evt) => this.handleIntro(evt, uri) }/>);
      } else {
        text.push(<span key={i}>{str[i]}</span>)
      }
    }
    this.setState({ text: text })

  }


  handleIntro(e, uri) {
    this.state.answersMap[uri].questionnaires_Value.stringValue = e.target.value;

    this.handleAdd()

  }

  handleAdd() {
    let map = this.state.answersMap;
    var keys = Object.keys(map);

    var values = keys.map(function (v) { return map[v]; });

    let questionAnswerObj = {
      optionsSelected: values,
      questionId: this.props.question.uri
    }

    let questionAnswer = new MODEL.InsertVariousTextsQuestionAnswer(questionAnswerObj)
    questionAnswer.genURI()

    this.props.addQuestionAnswer(questionAnswer)
  }

  render() {
    let question = this.props.question
    return (

      <Card className='game-card'>


        <CardTitle title={question.title} id={question.uri}/>

        <CardActions>

          {this.state.text}
          <br/>


        </CardActions>

      </Card>
    )
  }
}

export default VariousTextQuestion
