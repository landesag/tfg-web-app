import React, { Component } from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, History } from 'react-router'
import PropTypes from 'react-router';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


import ThemeDecorator from 'material-ui/styles/themeDecorator';

import injectTapEventPlugin from 'react-tap-event-plugin';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';

import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import LeftNav from 'material-ui/Drawer';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import MenuItem from 'material-ui/MenuItem';

import * as UIActions from '../../store/actions/UIActions'
import autobind from 'autobind-decorator'


import RadioButton from 'material-ui/RadioButton';
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup';

import * as MODEL from '../model'


class TrueFalseQuestion extends Component {


  constructor(props, context) {
    super(props, context)

    this.state = {
      answer: undefined,
      primaryTrue: false,
      primaryFalse: false
    }

    this.setTrue = this.setTrue.bind(this);
    this.setFalse = this.setFalse.bind(this);

  }


  componentDidMount() {
    console.log(this.props.question)


  }


  setTrue() {


    this.setState({
      primaryTrue: true,
      primaryFalse: false
    });

    let questionAnswerObj = {
      questionId: this.props.question.uri,
      optionSelected: this.props.question.options[0]
    }
    let questionAnswer = new MODEL.TrueFalseQuestionAnswer(questionAnswerObj)
    questionAnswer.genURI()

    this.props.addQuestionAnswer(questionAnswer)

  }


  setFalse() {

    this.setState({
      primaryTrue: false,
      primaryFalse: true
    });

    let questionAnswerObj = {
      questionId: this.props.question.uri,
      optionSelected: this.props.question.options[1]

    }

    let questionAnswer = new MODEL.TrueFalseQuestionAnswer(questionAnswerObj)
    questionAnswer.genURI()

    this.props.addQuestionAnswer(questionAnswer)

  }

  render() {

    let question = this.props.question
    return (

      <Card className='game-card'>

        {(() => {
          if (question.imageResources.length != 0 && question.imageResources.length == 1) {
            return (
              <CardMedia>
                <img src={question.imageResources[0]} />
              </CardMedia>
            )
          }
        })() }



        {(() => {
          if (question.videoResources.length != 0 && question.videoResources.length == 1) {

            return (
              <CardMedia>
                <video  controls>
                  <source src={question.videoResources[0]} type="video/mp4"/>
                  Your browser does not support the video tag.
                </video>
              </CardMedia>

            )
          }
        })() }

        <CardTitle title={question.title} id={question.uri}/>

        <CardActions>

          <br/>

          <RaisedButton primary = {this.state.primaryTrue} label="Verdadero" onClick={this.setTrue}/>
          <RaisedButton secondary = {this.state.primaryFalse} label="Falso" onClick={this.setFalse}/>


        </CardActions>

      </Card>
    )
  }
}

export default TrueFalseQuestion
