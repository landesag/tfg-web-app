import React, { Component } from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, History } from 'react-router'
import PropTypes from 'react-router';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


import ThemeDecorator from 'material-ui/styles/themeDecorator';

import injectTapEventPlugin from 'react-tap-event-plugin';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';

import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import LeftNav from 'material-ui/Drawer';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import MenuItem from 'material-ui/MenuItem';

import * as UIActions from '../../store/actions/UIActions'
import autobind from 'autobind-decorator'


import RadioButton from 'material-ui/RadioButton';
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup';

import * as MODEL from '../model'


class SingleAnswerQuestion extends Component {


  constructor(props, context) {
    super(props, context)
        this.changeActualAnswer = this.changeActualAnswer.bind(this);

  }


  componentDidMount() {
    console.log(this.props.question)

  }

  changeActualAnswer(event, value) {
    let optionObj = JSON.parse(value);

    let questionAnswerObj = {
      optionSelected: optionObj,
      questionId: this.props.question.uri
    }



    let questionAnswer = new MODEL.ChooseOneOptionQuestionAnswer(questionAnswerObj)
    questionAnswer.genURI()


    this.props.addQuestionAnswer(questionAnswer)

  }


  render() {

    let question = this.props.question
    return (
      

      <Card className='game-card'>



        <CardTitle title={question.title} id={question.uri}/>

        <CardActions>

           
          <RadioButtonGroup name="shipSpeed" onChange={this.changeActualAnswer}>
          
           {question.options.map(function (option, j) {
            return(
              <RadioButton
              key={j}
              value={JSON.stringify(option)}
              label={option.questionnaires_Value.stringValue}
              />   );
           }, this) }

          </RadioButtonGroup>

          <br/>



        </CardActions>

      </Card>
    )
  }
}

export default SingleAnswerQuestion
