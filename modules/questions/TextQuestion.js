import React, { Component } from 'react'
import { render } from 'react-dom'
import { Router, Route, Link, History } from 'react-router'
import PropTypes from 'react-router';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'


import ThemeDecorator from 'material-ui/styles/themeDecorator';

import injectTapEventPlugin from 'react-tap-event-plugin';

import Card from 'material-ui/Card/Card';
import CardActions from 'material-ui/Card/CardActions';
import CardHeader from 'material-ui/Card/CardHeader';
import CardMedia from 'material-ui/Card/CardMedia';
import CardTitle from 'material-ui/Card/CardTitle';

import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import LeftNav from 'material-ui/Drawer';
import TextField from 'material-ui/TextField';
import IconButton from 'material-ui/IconButton';
import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';

import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import MenuItem from 'material-ui/MenuItem';

import * as UIActions from '../../store/actions/UIActions'
import autobind from 'autobind-decorator'


import RadioButton from 'material-ui/RadioButton';
import RadioButtonGroup from 'material-ui/RadioButton/RadioButtonGroup';

import * as MODEL from '../model'


class TextQuestion extends Component {


  constructor(props, context) {
    super(props, context)
    this.state = {
      answer: undefined,
      text: []
    }
  }


  componentDidMount() {
    console.log(this.props.question)
    this.changeActualAnswer = this.changeActualAnswer.bind(this);
    this.handleIntro = this.handleIntro.bind(this);
    this.handleAdd = this.handleAdd.bind(this);


    var text = [];
    var pattext = "<_.*_>";
    var patt = new RegExp(pattext);

    var str = this.props.question.description.split(" ");
    for (var i = 0; i < str.length; i++) {

      if (patt.test(str[i])) {
        text.push(<TextField key={i} name = "ResponseTextField" value={this.state.answer} onChange={this.handleIntro}/>);
      } else {
        text.push(<span key={i}>{str[i]}</span>)
      }
    }
    this.setState({ text: text })

  }

  changeActualAnswer(event, value) {
    this.setState({
      answer: JSON.parse(value)
    });
  }

  handleIntro(e) {

    this.setState({
      answer: e.target.value
    });
    this.handleAdd(e.target.value)

  }

  handleAdd(valueAnswer) {

    let option = this.props.question.option;


    let stringObj = {
      stringValue: valueAnswer
    }

    let stringvalue = new MODEL.StringType(stringObj)
    stringvalue.genURI()

    option.questionnaires_Value = stringvalue;


    let questionAnswerObj = {
      optionSelected: option,
      questionId: this.props.question.uri
    }

    let questionAnswer = new MODEL.InsertOneTextQuestionAnswer(questionAnswerObj)
    questionAnswer.genURI()
    console.log(questionAnswer)

    this.props.addQuestionAnswer(questionAnswer)
  }

  render() {

    let question = this.props.question
    return (

      <Card className='game-card'>


        <CardTitle title={question.title} id={question.uri}/>

        <CardActions>

          {this.state.text}
          <br/>


        </CardActions>

      </Card>
    )
  }
}

export default TextQuestion
