import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Dimensions from 'react-dimensions'
import { Link } from 'react-router'
import { FormattedMessage } from 'react-intl'

import autobind from 'autobind-decorator'

import AppBar from 'material-ui/AppBar';
import LeftNav from 'material-ui/Drawer';
import IconButton from 'material-ui/IconButton';
import Divider from 'material-ui/Divider';
import FlatButton from 'material-ui/FlatButton';
import Snackbar from 'material-ui/Snackbar';
import CircularProgress from 'material-ui/CircularProgress';

import auth from '../auth'
import LinearProgress from 'material-ui/LinearProgress';

import NavigationMenu from 'material-ui/svg-icons/navigation/menu';
import Home from 'material-ui/svg-icons/action/home';
import Info from 'material-ui/svg-icons/action/info';
import Book from 'material-ui/svg-icons/action/book';
import Exit from 'material-ui/svg-icons/action/exit-to-app';
import Dialog from 'material-ui/Dialog';
import Badge from 'material-ui/Badge';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';
import AutoRenew from 'material-ui/svg-icons/action/autorenew';
import Person from 'material-ui/svg-icons/social/person';

import EditorInsertChart from 'material-ui/svg-icons/editor/insert-chart';

import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import DeveloperBoard from 'material-ui/svg-icons/hardware/developer-board.js';

import Avatar from 'material-ui/Avatar';

import * as UIActions from '../store/actions/UIActions'
import * as TaskActions from '../store/actions/TaskActions'
//Import here the different actions from components
//      import * as XXActions from '../../XX/Actions'


import { Breakpoints } from '../styles/Breakpoints'


/*
Componente principal de la aplicación una vez hecho Login y seleccionada la asignatura
*/
@Dimensions()
class App extends Component {
    constructor(props, context) {
        super(props, context);
    }


    //Se obtiene el router del contexto para las redirecciones automaticas
    static contextTypes = { router: React.PropTypes.object.isRequired };

    //Se establece el estado del componente
    state = {
        open: false, //Se pondrá a true cuando se seleccione logout y se abra el popup
        numberOfActiveTasks: 0
    }

    componentDidMount() {
        this.getUser()
        this.props.UIActions.setTitle(localStorage.subject)
        this.props.TaskActions.refreshAndGetTaskStart()
        this.props.TaskActions.reloadQuestionnaires()

    }
    //Funcion para obtener el propio usuario
    @autobind getUser() {

        let token = localStorage.token
        $.ajax({
            url: "https://tec.citius.usc.es/cuestionarios/backend/HMBOntologyRESTAPI/api/client/v2/user",
            dataType: 'json',
            headers: {
                'X-Auth-Token': token
            },
            success: function (data) {
                console.log(data)
                this.setState({
                    user: data.content
                });

            }.bind(this),
            error: function (xhr, status, err) {
                if (xhr.status == 403) {
                    auth.logout()
                    this.context.router.replace('/login')
                }
            }.bind(this)
        });
    }



    @autobind handleNavigationButtonClick() {
        this.toggleMenu(true);
    }

    @autobind toggleMenu(force = false) {
        if (this.props.isMenuOpened || force)
            this.props.UIActions.toggleMenu()
    }

    @autobind closeSnack() {
        if (this.props.isSnackbarOpened)
            this.props.UIActions.closeSnack()
    }

    @autobind changeSubject() {

        localStorage.removeItem("subject")
        this.context.router.replace('/chooseSubject')

    }


    handleClose = () => {
        this.setState({
            open: false
        });
    };

    handleOpen = () => {
        this.setState({
            open: true
        });
    };

    exit = () => {
        console.log('saliendo')

        auth.logout()
        this.context.router.replace('/login')

    };

    @autobind reload() {
        this.props.TaskActions.refreshAndGetTaskStart()
        this.props.TaskActions.reloadQuestionnaires()


    }

    render() {

        const { numberOfActiveTasks, isLookingForTasks, title, isMenuOpened, isSnackbarOpened, snackbarMessage, children, containerWidth } = this.props;

        let userName;
        let mail;

        if (this.state.user != undefined) {
            userName = this.state.user.completeName
            mail = this.state.user.email
        } else {
            userName = "-"
            mail = "-"
        }
        const actions = [
            <FlatButton
                label="SI"
                secondary={false}
                onClick={this.exit}
                />,
            <FlatButton
                label="NO"
                secondary={true}
                onClick={this.handleClose}
                />,
        ];

        var buttonStyle = {
            backgroundColor: 'transparent',
            color: 'white'
        };

        var buttonContStyle = {
            top: '7px',
            left: '3px'
        };
        return (
            <div className='app-container'>


                <Dialog
                    title="¿Seguro que quieres salir?"
                    actions={actions}
                    modal={false}
                    open={this.state.open}
                    onRequestClose={this.handleClose}
                    >
                    Se cerrará la sesión actual.
                </Dialog>

                <AppBar
                    className = 'appbar-container'
                    title = {title}
                    iconElementLeft = {
                        <IconButton onClick={ this.handleNavigationButtonClick }>
                            <NavigationMenu />
                        </IconButton>
                    }
                    iconElementRight = {
                        <IconButton onClick={this.reload}>
                            <AutoRenew color="ffffff" />
                        </IconButton>

                    }

                    showMenuIconButton = { containerWidth < Breakpoints.large } >



                    {(() => {
                        if (!isLookingForTasks) {
                            return (
                                <span className="badge">{numberOfActiveTasks} tareas</span>
                            )
                        }
                    })() }
                    <IconMenu

                        iconButtonElement={<IconButton style ={buttonContStyle} iconStyle = {buttonStyle}>
                            <MoreVertIcon />
                        </IconButton>}
                        anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                        targetOrigin={{ horizontal: 'left', vertical: 'top' }}
                        >

                        <MenuItem leftIcon={<Book />}  onClick={this.changeSubject}>
                            Cambiar asignatura
                        </MenuItem>
                        <MenuItem leftIcon={<Exit />}  onClick={this.handleOpen}>
                            <FormattedMessage
                                id='main.menu.home'
                                description='Salir de la aplicación'
                                defaultMessage='Logout'
                                />
                        </MenuItem>
                    </IconMenu>

                </AppBar>

                {(() => {
                    if (isLookingForTasks) {
                        return (
                            <LinearProgress/>
                        )
                    }
                })() }

                <LeftNav
                    className='leftbar-container'
                    open={isMenuOpened || (containerWidth >= Breakpoints.large) }
                    docked={ containerWidth >= Breakpoints.large }
                    onRequestChange={ this.toggleMenu }

                    >

                    <img src="../imgs/logo-student-project.png" className="logo"/>

                    <Divider/>

                    <div className = "userItem" style={{ fontSize: '17px' }}>
                        <br/>
                        <Person/>
                        <br/>
                        <div>
                            {userName} <br/>{mail}
                        </div>
                        <br/>
                    </div>

                    <Divider/>

                    <Link to='/' onClick={this.toggleMenu}>
                        <MenuItem leftIcon={<Home />}>
                            <FormattedMessage
                                id='main.menu.home'
                                description='Main menu home link'
                                defaultMessage='Home'
                                />
                        </MenuItem>
                    </ Link>

                    <Link to='/history' onClick={ this.toggleMenu }>
                        <MenuItem leftIcon={<EditorInsertChart />}>
                            <FormattedMessage
                                id='main.menu.stats'
                                description='Link a estadísticas'
                                defaultMessage='Estadísticas'
                                />
                        </MenuItem>
                    </ Link>

                </LeftNav>
                <div className='main-container'>
                    { children }
                </div>

                <Snackbar
                    open={isSnackbarOpened}
                    message={snackbarMessage}
                    autoHideDuration={4000}
                    onRequestClose={this.closeSnack}
                    />

            </div>
        )
    };

    static propTypes = {
        title: PropTypes.string.isRequired,
        isMenuOpened: PropTypes.bool.isRequired,
        isSnackbarOpened: PropTypes.bool.isRequired,
        snackbarMessage: PropTypes.string.isRequired,
        children: PropTypes.node.isRequired,
        tasks: PropTypes.array.isRequired,
        isLookingForTasks: PropTypes.bool.isRequired,
        numberOfActiveTasks: PropTypes.number.isRequired
    };
}

function mapStateToProps(state) {
    return {
        title: state.UIState.title,
        isMenuOpened: state.UIState.isMenuOpened,
        isSnackbarOpened: state.UIState.isSnackbarOpened,
        snackbarMessage: state.UIState.snackbarMessage,
        tasks: state.TaskState.tasks,
        isLookingForTasks: state.TaskState.isLookingForTasks,
        numberOfActiveTasks: state.TaskState.numberOfActiveTasks

    };
}

function mapDispatchToProps(dispatch) {
    return {
        UIActions: bindActionCreators(UIActions, dispatch),
        TaskActions: bindActionCreators(TaskActions, dispatch),
        //Add here the binding for the different actions
        //      XXActions: bindActionCreators(XXActions, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
