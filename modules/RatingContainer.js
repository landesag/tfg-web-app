import React, { Component, PropTypes } from 'react'


import * as UIActions from '../store/actions/UIActions'

import Dimensions from 'react-dimensions'

@Dimensions()
class RatingContainer extends Component {


  constructor(props, context) {
    super(props, context)
  }



  render() {
    let widthstyle = this.props.rating / this.props.max_rating * 100 + "%"
    var divStyle = {
      width: widthstyle
    };


    return (

      <div className = "boxRating"><div className="boxRatingText">{this.props.rating}/{this.props.max_rating}</div><div style={divStyle}className="ratingFill"></div></div>
    )
  }


  static propTypes = {
    max_rating: PropTypes.number.isRequired,
    rating: PropTypes.number.isRequired
  };

}

export default RatingContainer