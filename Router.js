import React from 'react'
import { Route, IndexRoute, Redirect } from 'react-router'
import auth from './auth'

//Import here the different components

import History from './modules/History'
import Test from './modules/Test'
import Home from './modules/Home'
import Login from './modules/Login'
import UnitHistory from './modules/UnitHistory'
import App from './modules/App'
import ChooseSubject from './modules/ChooseSubject'
import Repos from './modules/Repos'
import My404Component from './modules/My404Component'

function requireAuthAndSubject(nextState, replace) {
  if (!auth.loggedIn())
    replace('/login')

  if (!localStorage.subject)
    replace('/chooseSubject')

}


function requireAuthAndSubjectAndTask(nextState, replace) {
  if (!auth.loggedIn())
    replace('/login')

  if (!localStorage.subject)
    replace('/chooseSubject')

  if (!localStorage.task)
    replace('/')
}

function requireAuth(nextState, replace) {
  if (!auth.loggedIn())
    replace('/login')
}

function requireNotAuth(nextState, replace) {
  if (auth.loggedIn())
    replace('/')
}

export default (

  <Route path=''>

    <Route path="/login" component={Login}  onEnter={requireNotAuth}/>
    <Route path ="/chooseSubject" component={ChooseSubject} onEnter={requireAuth}/>
    <Route path="/" component={App} onEnter={requireAuthAndSubject}  name="App">
      <IndexRoute component={Home}/>
      <Route path="history"  name="Estadísticas de la asignatura">
        <IndexRoute component={History}/>
        <Route path=":unitID" component={UnitHistory} />
      </Route>

    </Route>
    <Route path="/task" component={Test} onEnter={requireAuthAndSubjectAndTask}/>

    <Route path='/404' component={My404Component} />
    <Redirect from='*' to='/404' />

  </Route>


)
