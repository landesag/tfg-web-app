module.exports = {
  login(email, pass, cb) {
    cb = arguments[arguments.length - 1]
    if (localStorage.token) {
      if (cb) cb(true)
      this.onChange(true)
      return
    }
    pretendRequest(email, pass, (res) => {
      if (res.authenticated) {
        localStorage.token = res.token
        if (cb) cb(true)
        this.onChange(true)
      } else {
        if (cb) cb(false)
        this.onChange(false)
      }
    })
  },

  getToken() {
    return localStorage.token
  },

  logout(cb) {
    delete localStorage.token
    delete localStorage.subject
    if (cb) cb()
    this.onChange(false)
  },

  loggedIn() {
    return !!localStorage.token
  },

  onChange() {}
}

function pretendRequest(email, pass, cb) {

    $.ajax({
      url: 'http://tec.citius.usc.es/cuestionarios/backend/HMBAuthenticationRESTAPI/auth/login',
      data: {"username": email,"password":pass},
      dataType: 'json',
      success: function(data) {
        cb({
          authenticated: true,
          token: data.content
        })
      }.bind(this),
      error: function(xhr, status, err) {
        cb({ authenticated: false })
      }.bind(this)
    });

}
