import TYPES from '../actions/types/TasksActionTypes'
const InitialState = {
    tasks: [],
    numberOfActiveTasks: 0,
    isLookingForTasks: false
}

export default function TaskReducer(state = InitialState, action = { type: '', payload: {} }) {
    switch (action.type) {
        case TYPES.REFRESH_AND_GET_TASKS_START:
            return {
                ...state,
                isLookingForTasks: true
            }
        case TYPES.REFRESH_AND_GET_TASKS_FINISHED:
            return {
                ...state,
                tasks: action.payload.tasks,
                numberOfActiveTasks: action.payload.numberOfActiveTasks,
                isLookingForTasks: false
            }
        default:
            return state
    }
}


