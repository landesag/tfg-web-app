import UIReducer from './UIReducer'
import TaskReducer from './TaskReducer'
//Import reducers here:
//      import XXReducer from '../../../components/XX/Reducer'

const reducers = {
    UIState: UIReducer,
    TaskState: TaskReducer,
    //Include reducers here:
    //      XXState : XXReducer
};

export default reducers;
