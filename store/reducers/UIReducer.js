import TYPES from '../actions/types/UIActionTypes'

const InitialState = {
    isMenuOpened: false,
    title: 'ge learn',
    isSnackbarOpened: false,
    snackbarMessage: 'Default message'
}

export default function UIReducer(state = InitialState, action = { type: '', payload: {} }) {
    switch (action.type) {
        case TYPES.TOGGLE_MENU:
            return {
                ...state,
                isMenuOpened: !state.isMenuOpened
            }
        case TYPES.SET_TITLE:
            return {
                ...state,
                title: action.payload.title
            };
        case TYPES.OPEN_SNACK:
            return {
            ...state,
                snackbarMessage: action.payload.snackbarMessage,
                isSnackbarOpened: true
            }
        case TYPES.CLOSE_SNACK:
            return {
            ...state,
                isSnackbarOpened: false
            }

        default:
            return state
    }
}