import UI_ACTION_TYPES from './types/UIActionTypes'

export function toggleMenu() {
    return { type: UI_ACTION_TYPES.TOGGLE_MENU }
}
export function openSnack(snackbarMessage) {
    console.log(snackbarMessage)
    return { type: UI_ACTION_TYPES.OPEN_SNACK, payload: { snackbarMessage } }
}
export function closeSnack() {
    return { type: UI_ACTION_TYPES.CLOSE_SNACK }
}
export function setTitle(title) {
    return { type: UI_ACTION_TYPES.SET_TITLE, payload: { title } }
}