export const TYPES = {
    TOGGLE_MENU: Symbol('TOGGLE_MENU'),
    SET_TITLE: Symbol('SET_TITLE'),
    OPEN_SNACK: Symbol('OPEN_SNACK'),
    CLOSE_SNACK: Symbol('CLOSE_SNACK')

}

export default TYPES;