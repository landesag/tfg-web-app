import TASK_ACTION_TYPES from './types/TasksActionTypes'
import ReduxThunk from 'redux-thunk' // no changes here 😀

export function refreshAndGetTaskStart() {
    return { type: TASK_ACTION_TYPES.REFRESH_AND_GET_TASKS_START }
}

export function refreshAndGetTaskFinished(tasks, numberOfActiveTasks) {
    console.log('Tareas recuperadas')
    console.log(tasks)
    console.log(numberOfActiveTasks)
    return { type: TASK_ACTION_TYPES.REFRESH_AND_GET_TASKS_FINISHED, payload: { tasks, numberOfActiveTasks } }
}



export function reloadQuestionnaires() {

    console.log("Recargando cuestionarios")

    return function (dispatch) {

        let token = localStorage.token
        $.ajax({
            url: "https://tec.citius.usc.es/cuestionarios/backend/HMBOntologyRESTAPI/api/client/v2/workflows/page/0/size/50",
            dataType: 'json',
            headers: {
                'X-Auth-Token': token
            },
            success: function (data) {

                activateSegmentation(dispatch, 0, data.content.result)

            }.bind(this),
            error: function (xhr, status, err) {
            }.bind(this)
        });
    }

}
function activateSegmentation(dispatch, counter, segmentationIds) {

    console.log('lol')
    if (counter === undefined)
        counter = 0;
    if (counter >= segmentationIds.length) {
        reloadCase(dispatch, 0, segmentationIds);
        return;
    }
    if (!segmentationIds[counter].accepted) {
        let segmentationId = segmentationIds[counter]._id

        let token = localStorage.token
        $.ajax({
            url: "https://tec.citius.usc.es/cuestionarios/backend/HMBWFExecutionEngineRESTAPI/api/client/v2/segmentation/" + segmentationId + "/accept",
            dataType: 'json',
            headers: {
                'X-Auth-Token': token
            },
            success: function (data) {
                counter++;
                activateSegmentation(dispatch, counter, segmentationIds);


            }.bind(this),
            error: function (xhr, status, err) {
                counter++;
                activateSegmentation(dispatch, counter, segmentationIds);
            }.bind(this)
        });
    } else {
        counter++;
        activateSegmentation(dispatch, counter, segmentationIds);
    }


}

function reloadCase(dispatch, counter, result) {
    if (counter === undefined)
        counter = 0;
    if (counter >= result.length) {
        getTasks(dispatch, [], 0);
        return;
    }


    let caseId = result[counter].caseId
    let executionId = result[counter].executionId

    let token = localStorage.token
    $.ajax({
        url: "https://tec.citius.usc.es/cuestionarios/backend/" +
        "HMBWFExecutionEngineRESTAPI/api/client/v2/execution/" + executionId + "/case/" + caseId + "/reevaluate",
        dataType: 'json',
        headers: {
            'X-Auth-Token': token
        },
        success: function (data) {
            counter++
            reloadCase(dispatch, counter, result)

        }.bind(this),
        error: function (xhr, status, err) {

            counter++
            reloadCase(dispatch, counter, result)
        }.bind(this)
    });
}


function getTasks(dispatch, tasks = [], page = 0) {

    let token = localStorage.token
    $.ajax({
        url: "https://tec.citius.usc.es/cuestionarios/backend/HMBOntologyRESTAPI/api/client/v2/tasks/page/" + page + "/size/50",
        dataType: 'json',
        headers: {
            'X-Auth-Token': token
        },
        success: function (data) {
            if (data.content.numberOfPages - 1 > page) {
                getTasks(dispatch, tasks, page + 1)
            } else {

                let total = 0;
                for (var i = 0; i < data.content.result.length; i++) {
                    for (var j = 0; j < data.content.result[i].taskList.length; j++) {
                        total++;
                    }
                }

                dispatch(refreshAndGetTaskFinished(data.content.result, total))
            }

        }.bind(this),
        error: function (xhr, status, err) {
        }.bind(this)
    });
}

    ///////////////////////////////////
